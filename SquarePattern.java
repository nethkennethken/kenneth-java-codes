package devkinetics;

import java.util.Scanner;

public class SquarePattern {
    public static void main(String[] args){
        int size, choice, row1 = 1, column1 = 1;
        
        Scanner getInput = new Scanner(System.in);
        
        System.out.println("Make a square using");
        System.out.println("1. for loop");
        System.out.println("2. while loop");
        System.out.print("Choice: ");
        choice = getInput.nextInt();
        
        switch(choice){
            case 1:
                System.out.print("Enter the size: ");
                size = getInput.nextInt();
                for(int row = 1; row <= size; row++){
                    for(int col = 1; col <= size; col++){
                        System.out.print("# ");
                    }
                    System.out.println();
                }
                
                break;
            case 2:
                System.out.print("Enter the size: ");
                size = getInput.nextInt();
                while(row1 <= size){
                    column1 = 1;
                    while(column1 <= size){
                        System.out.print("# ");
                        ++column1;
                    }
                    ++row1;
                    System.out.println();
                }
                
                break;
        }
        
    }
}
