package devkinetics;

import java.util.Scanner;

public class SalesTaxCalculator {
    public static void main(String[] args){
        final double SALES_TAX_RATE = 0.07;
        final int SENTINEL = -1;
        
        Scanner getInput = new Scanner(System.in);
        
        double price, actualPrice, salesTax;
        double totalPrice = 0.0, totalActualPrice = 0.0, totalSalesTax = 0.0;
        
        System.out.print("Enter the tax-inclusive price in dollars "
                + "(or -1 to end): $");
        price =  getInput.nextDouble();
        
        while(price != SENTINEL){
            salesTax = price * SALES_TAX_RATE;
            actualPrice = price - salesTax;
            
            totalPrice += price;
            totalActualPrice += actualPrice;
            totalSalesTax += salesTax;
            
            System.out.printf("Actual Price is: $%.2f", actualPrice);
            System.out.printf(", Sales Tax is: $%.2f%n", salesTax);
            System.out.println();
            
            System.out.print("Enter the tax-inclusive price in dollars "
                    + "(or -1 to end): $");
            price =  getInput.nextDouble();
        }
        
        System.out.printf("Total Price is: $%.2f%n", totalPrice);
        System.out.printf("Total Actual Price is: $%.2f%n", totalActualPrice);
        System.out.printf("Total Sales Tax is: $%.2f%n", totalSalesTax);
        
        getInput.close();
    }
}
