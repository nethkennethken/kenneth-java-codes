package devkinetics;

import java.util.Scanner;

public class TriangularPatternC {
    public static void main(String[] args){
        int size;
        
        Scanner getSize = new Scanner(System.in);
        
        System.out.print("Enter the size: ");
        size = getSize.nextInt();
        
        for(int row = size; row >= 1; row--){
            for(int col = size; col >= 1; col--){
                if (row >= col) {
                    System.out.print("# ");
                } else {
                    System.out.print("  ");
                }
            }
            System.out.println();
        }
    }
}
