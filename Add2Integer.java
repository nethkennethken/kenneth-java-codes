package devkinetics;

import java.util.Scanner;

public class Add2Integer {
    public static void main(String[] args){
        int number1, number2, sum;
        
        Scanner getNumber = new Scanner(System.in);
        
        System.out.print("Enter first number: ");
        number1 = getNumber.nextInt();
        System.out.print("Enter second number: ");
        number2 = getNumber.nextInt();
        
        getNumber.close();
        
        sum = number1 + number2;
        
        System.out.println("The sum is: " + sum);
    }
}
