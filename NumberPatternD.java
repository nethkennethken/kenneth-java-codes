package devkinetics;

import java.util.Scanner;

public class NumberPatternD {
    public static void main(String[] args){
        int size, number, number2;
        
        Scanner getSize = new Scanner(System.in);
        
        System.out.print("Enter the size: ");
        size = getSize.nextInt();
        number = size;
        number2 = size;
        
        for(int row = 1; row <= size; row++){
            for(int col = 1; col <= size; col++){
                if (row + col <= size + 1) {
                    System.out.print(number + " ");
                    number--;
                } else {
                    System.out.print("  ");
                }
            }
            number2--;
            number = number2;
            System.out.println();
        }
    }
}
