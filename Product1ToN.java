package devkinetics;

import java.util.Scanner;

public class Product1ToN {
    static int intProduct = 1;
    static long longProduct = 1;
    static final int LOWERBOUND = 1;
    static final int UPPERBOUND = 10;
    
    public static void main(String[] args){
        int choice;
        int newUPPERBOUND;
        
        Scanner getInput = new Scanner(System.in);
        
        
        System.out.println("1. Factorial of 10");
        System.out.println("2. Factorial of your choice");
        System.out.println("3. Factorial of your choice (compare int and long)");
        System.out.println();
        System.out.print("Choice: ");
        choice = getInput.nextInt();
        
        switch(choice){
            case 1:
                for(int number = LOWERBOUND; number <= UPPERBOUND; ++number){
                    intProduct *= number;
                }
                System.out.println("Result: " + intProduct);
                break;
            case 2:
                System.out.print("Enter the number you want to factor: ");
                newUPPERBOUND = getInput.nextInt();
                
                for(int number = LOWERBOUND; number <= newUPPERBOUND; ++number){
                    intProduct *= number;
                }
                System.out.println("Result: " + intProduct);
                break;
            case 3:
                System.out.print("Enter the number you want to factor: ");
                newUPPERBOUND = getInput.nextInt();
                
                for(int number = LOWERBOUND; number <= newUPPERBOUND; ++number){
                    intProduct *= number;
                }
                
                for(int number = LOWERBOUND; number <= newUPPERBOUND; ++number){
                    longProduct *= number;
                }
                System.out.println("Result (int): " + intProduct);
                System.out.println("Result (long): " + longProduct);
                break;
        }
        
        getInput.close();
    }    
}
