package devkinetics;

import java.util.Scanner;

public class CheckOddEven {
    public static void main(String[] args){
        int number;
        Scanner getNum = new Scanner(System.in);
        
        System.out.print("Enter a number: ");
        number = getNum.nextInt();
        
        getNum.close();
        
        System.out.println();
        
        System.out.println("The number is " + number);
        System.out.print("Remark: ");
        
        if((number%2) == 0){
            System.out.println("Even Number");
        }
        else{
            System.out.println("Odd Number");
        }
        System.out.println("bye!");
    }
}
