package devkinetics;

import java.util.Scanner;

public class ExtractDigits {
    public static void main(String[] args){
        int n = 0;
        
        Scanner getNumber = new Scanner(System.in);
        
        System.out.print("Reverse this number: ");
        n = getNumber.nextInt();
        System.out.print("Reversed: ");
        while(n>0){
            int digit = n % 10;
            System.out.print(digit);
            
            n = n / 10;
        }
        System.out.println(" ");
        
        getNumber.close();
    }
}
