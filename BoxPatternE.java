package devkinetics;

import java.util.Scanner;

public class BoxPatternE {
    public static void main(String[] args){
        int size;
        
        Scanner getSize = new Scanner(System.in);
        
        System.out.print("Enter the size: ");
        size = getSize.nextInt();
        
        for(int row = 1; row <= size; row++){
            for(int col = 1; col <= size; col++){
                if(row == 1 || row == size || row + col == size + 1 
                        || row == col || col == 1 || col == size){
                    System.out.print("# ");
                }else{
                    System.out.print("  ");
                }
            }
            System.out.println();
        }
    }
}
