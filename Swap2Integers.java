package devkinetics;

import java.util.Scanner;

public class Swap2Integers {
    public static void main(String[] args){
        int number1, number2, temp;
        
        Scanner getInput  = new Scanner(System.in);
        
        System.out.print("Enter first integer: ");
        number1 = getInput.nextInt();
        System.out.print("Enter second integer: ");
        number2 = getInput.nextInt();
        
        temp = number1;
        number1 = number2;
        number2 = temp;
        
        System.out.println("After the swap, first integer is: " + number1 + 
                ", second integer is: " + number2);
        
        getInput.close();
    }
}
