package devkinetics;

public class Fibonacci {
    public static void main(String[] args){
        int n = 3;
        int fn;
        int fnMinus1 = 1;
        int fnMinus2 = 1;
        double nMax = 20;
        int sum = fnMinus1 + fnMinus2;
        double average;
        
        System.out.println("The first " + nMax + " Fibonacci numbers are:");
        System.out.print(fnMinus1 + " " + fnMinus2);
        
        while(n <= nMax){
            fn = fnMinus1 + fnMinus2;
            
            ++n;
            System.out.print(" " + fn);
            fnMinus1 = fnMinus2;
            fnMinus2 = fn;
            sum += fn;
        }
        
        System.out.println();
        average = sum / nMax;
        System.out.println("The average is " + average);
    }
}
