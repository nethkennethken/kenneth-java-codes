package devkinetics;

public class Factorial {
   public static void main(String[] args) {  // Set an initial breakpoint at this statement
      int n = 20;
      float factorial = 1;

      // n! = 1*2*3...*n
      for (int i = n; i >= 1; i--) {  // i = 1, 2, 3, ..., n
         factorial = factorial * i;   // *
      }
      System.out.println("The Factorial of " + n + " is " + factorial);
   }
}
