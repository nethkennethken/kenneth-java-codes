package devkinetics;

import java.util.Scanner;

public class AverageWithInputValidation {
    public static void main(String[] args){
        final double NUM_STUDENTS = 3;
        
        int numberIn;
        boolean isValid;
        double sum = 0;
        double average;
        
        Scanner getInput = new Scanner(System.in);
        
        for(int studNo = 1; studNo <= NUM_STUDENTS; ++studNo){
            do{
                System.out.print("Enter the mark (0-100) for student " + studNo 
                        + ": ");
                numberIn = getInput.nextInt();

                isValid = numberIn >= 0 && numberIn <= 100;
                if(!isValid){
                    System.out.println("Invalid input, try again...");
                }
            }while(!isValid);
            sum += numberIn;
        }
        average = sum / NUM_STUDENTS;
        System.out.printf("The average is: %.2f%n", average);
    }
}
