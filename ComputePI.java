package devkinetics;

import java.util.Scanner;

public class ComputePI {
    public static void main(String[] args){
        double sum = 0.0;
        double piComputed;
        int[] MAX_DENOMINATOR = {1000, 10000, 100000, 1000000};
        int MAX_TERM = 10000;
        int choice = 0;
        
        
        Scanner getChoice = new Scanner(System.in);
        
        System.out.println("What terminating condition do you want to use?");
        System.out.println("1. MAX_DENOMINATOR");
        System.out.println("2. MAX_TERM (10000)");
        System.out.println("Choice: ");
        choice = getChoice.nextInt();
        System.out.println();
        
        switch(choice){
            case 1:
                System.out.println("Please choose the MAX DENOMINATOR you want "
                + "to use: ");
                System.out.println("1. 1000");
                System.out.println("2. 10000");
                System.out.println("3. 100000");
                System.out.println("4. 1000000");
                System.out.print("Choice: ");
                choice = getChoice.nextInt();
                System.out.println();

                for(double denominator = 1; denominator <= MAX_DENOMINATOR[choice-1]; 
                        denominator += 2){
                    if(denominator % 4 == 1){
                        sum += 1 / denominator;
                    }
                    else if(denominator % 4 == 3){
                        sum -= 1 / denominator;
                    }
                    else{
                        System.out.println("Impossible!!!");
                    }
                }
                System.out.println("PI Obtained: " + (4 *sum));
                
                break;
            case 2:
                for(double term = 1; term <= MAX_TERM; term++){
                    if(term % 2 == 1){
                        sum += 1.0 / (term * 2 - 1);
                    }
                    else{
                        sum -= 1.0 / (term * 2 - 1);
                    }
                }
                piComputed = 4 *sum;
                System.out.println("PI Obtained: " + piComputed);
                System.out.println("Built-in PI Value: " + Math.PI);
                System.out.println("Similarity: " + (piComputed / Math.PI) 
                        * 100 + "%");
                
                break;
        }
        
        getChoice.close();
    }
}
