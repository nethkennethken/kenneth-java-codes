package devkinetics;

import java.util.Scanner;

public class CircleComputation {
    public static void main(String[] args){
        double radius, diameter, circumference, area, surfaceArea, volume, 
                baseArea, height;
        int choice;
        
        Scanner getInput = new Scanner(System.in);
        
        System.out.println("Select kind of computation");
        System.out.println("1. Circle Computation");
        System.out.println("2. Sphere Computation");
        System.out.println("3. Cylinder Computation");
        System.out.print("Choice: ");
        
        choice = getInput.nextInt();
        System.out.println();
        
        switch(choice){
            case 1:
                System.out.print("Enter the radius: ");
                radius = getInput.nextDouble();

                diameter = 2.0 * radius;
                area = Math.PI * radius * radius;
                circumference = 2.0 * Math.PI * radius;

                System.out.printf("Diameter is: %.2f%n", diameter);
                System.out.printf("Area is: %.2f%n", area);
                System.out.printf("Circumference is: %.2f%n", circumference);
                
                break;
            case 2:
                System.out.print("Enter the radius: ");
                radius = getInput.nextDouble();
                
                surfaceArea = 4 * Math.PI * radius * radius;
                volume = 4 /3 * Math.PI * radius * radius * radius;

                System.out.printf("Surface Area is: %.2f%n", surfaceArea);
                System.out.printf("Volume is: %.2f%n", volume);
                
                break;
            case 3:
                System.out.print("Enter the radius: ");
                radius = getInput.nextDouble();
                System.out.print("Enter the height: ");
                height = getInput.nextDouble();

                baseArea = Math.PI * radius * radius;
                surfaceArea = 2.0 * Math.PI * radius + 2.0 * baseArea;
                volume = baseArea * height;

                System.out.printf("Base Area is: %.2f%n", baseArea);
                System.out.printf("Surface Area is: %.2f%n", surfaceArea);
                System.out.printf("Volume is: %.2f%n", volume);
                
                break;
        }
        
        getInput.close();
    }
}
