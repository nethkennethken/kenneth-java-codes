package devkinetics;

public class HarmonicSum {
    static final int MAX_DENOMINATOR = 50000;
    
    public static void main(String[] args){
        double sumL2R = 0;
        double sumR2L = 0;
        double absDiff;
        double temp;
        
        for(double denominator = 1; denominator <= MAX_DENOMINATOR; ++denominator){
            temp = 1 / denominator;
            sumL2R += temp;
        }
        System.out.println("The sum from left-to-right is: " + sumL2R);
        
        for(double denominator = MAX_DENOMINATOR; denominator >= 1; --denominator){
            temp = 1 / denominator;
            sumR2L += temp;
        }
        System.out.println("The sum from right-to-left is: " + sumR2L);
        
        if(sumL2R > sumR2L){
            absDiff = sumL2R - sumR2L;
        }
        else{
            absDiff = sumR2L - sumL2R;
        }
        System.out.println("Absolute difference: " + absDiff);
    }
}
