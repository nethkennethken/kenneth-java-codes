package devkinetics;

import java.util.Scanner;

public class NumberPatternC {
    public static void main(String[] args){
        int size, number, number2;
        
        Scanner getSize = new Scanner(System.in);
        
        System.out.print("Enter the size: ");
        size = getSize.nextInt();
        number = 1;
        number2 = 1;
        
        for(int row = size; row >= 1; row--){
            for(int col = size; col >= 1; col--){
                if (row + col <= size + 1) {
                    System.out.print(number + " ");
                    number--;
                } else {
                    System.out.print("  ");
                }
            }
            number2++;
            number = number2;
            System.out.println();
        }
    }
}
