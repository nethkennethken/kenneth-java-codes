package devkinetics;

import java.util.Scanner;

public class CheckerPattern {
    public static void main(String[] args){
        int size;
        String hashtag;
        
        Scanner getSize = new Scanner(System.in);
        
        System.out.print("Enter the size: ");
        size = getSize.nextInt();
        
        for (int row = 1; row <= size; row++){
            for (int col = 1; col <= size; col++) {
                if((row % 2) == 0){
                    hashtag = " #";
                }else{
                    hashtag = "# ";
                }
                System.out.print(hashtag);
            }
            System.out.println();
        }
    }
}
