package devkinetics;

import java.util.Scanner;

public class NumberPatternB {
    public static void main(String[] args){
        int size, number = 0;
        
        Scanner getSize = new Scanner(System.in);
        
        System.out.print("Enter the size: ");
        size = getSize.nextInt();
        
        for(int row = size; row >= 1; row--){
            for(int col = size; col >= 1; col--){
                if (row >= col) {
                    ++number;
                    System.out.print(number + " ");
                } else {
                    System.out.print("  ");
                }
            }
            number = 0;
            System.out.println();
        }
    }
}
