package devkinetics;

import java.util.Scanner;

public class IncomeTaxCalculatorWithSentinel {
    public static void main(String[] args){
        final double TAX_RATE_ABOVE_20K = 0.1;
        final double TAX_RATE_ABOVE_40K = 0.2;
        final double TAX_RATE_ABOVE_60K = 0.3;
        final int SENTINEL = -1;
        
        int taxableIncome = 0;
        double taxPayable = 0;
        
        Scanner getIncome = new Scanner(System.in);
        
        System.out.print("Enter the taxable income (or -1 to end): $");
        taxableIncome = getIncome.nextInt();

        while(taxableIncome != SENTINEL){
            if (taxableIncome <= 20000) {
            taxPayable = taxableIncome * 0;
            } else if (taxableIncome <= 40000) {
                taxableIncome -= 20000;
                taxPayable += taxableIncome * TAX_RATE_ABOVE_20K;
            } else if (taxableIncome <= 60000) {
                taxableIncome -= 20000;
                taxableIncome -= 20000;
                taxPayable += 20000 * TAX_RATE_ABOVE_20K;
                taxPayable += taxableIncome * TAX_RATE_ABOVE_40K;
            } else {
                taxableIncome -= 20000;
                taxableIncome -= 20000;
                taxableIncome -= 20000;
                taxPayable += 20000 * TAX_RATE_ABOVE_20K;
                taxPayable += 20000 * TAX_RATE_ABOVE_40K;
                taxPayable += taxableIncome * TAX_RATE_ABOVE_60K;
            }

            System.out.printf("The income tax payable is: $%.2f%n", 
                    taxPayable);
            
            taxPayable = 0;
            
            System.out.println();
            System.out.print("Enter the taxable income (or -1 to end): $");
            taxableIncome = getIncome.nextInt();
        }
        System.out.println("bye!");
        getIncome.close();
    }
}
