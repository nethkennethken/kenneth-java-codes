package devkinetics;

import java.util.Scanner;

public class PrintDayInWord {
    public static void main(String[] args){
        int dayNumber;
        Scanner getDay = new Scanner(System.in);
        
        System.out.print("Enter a number: ");
        dayNumber = getDay.nextInt();
        System.out.println();
        
        switch(dayNumber){
            case 1:
                System.out.println("SUNDAY");
                break;
            case 2:
                System.out.println("MONDAY");
                break;
            case 3:
                System.out.println("TUESDAY");
                break;
            case 4:
                System.out.println("WEDNESDAY");
                break;
            case 5:
                System.out.println("THURSDAY");
                break;
            case 6:
                System.out.println("FRIDAY");
                break;
            case 7:
                System.out.println("SATURDAY");
                break;
            default:
                System.out.println("Not a valid day");
                break;
        }
        
        getDay.close();
    }
}