package devkinetics;

import java.util.Scanner;

public class SumAverageRunningInt {
    static final int LOWERBOUND = 1;
    static final int UPPERBOUND = 100;
    static final int LOWERBOUND1 = 111;
    static final int UPPERBOUND1 = 8899;
    
    public static void main(String[] args){
        int sum = 0;
        int choice, count = 0;
        double average;
        
        Scanner getChoice = new Scanner(System.in);
    
        System.out.println("Choose what you want to do:");
        System.out.println("1. Produce the sum of 1, 2, 3, ..., to 100 and "
                + "compute the Average");
        System.out.println("2. Use a \"while-do\" loop");
        System.out.println("3. Use a \"do-while\" loop");
        System.out.println("4. Make the program sum from 111 to 8899, "
                + "and compute the average");
        System.out.println("5. Find the \"sum of the squares\" of all the "
                + "numbers from 1 to 100");
        System.out.println("6. Produce two sums: sum of odd numbers "
                + "and sum of even numbers from 1 to 100");
        System.out.println();
        
        System.out.print("Your choice: ");
        choice = getChoice.nextInt();
        System.out.println();
        
        switch(choice){
            case 1:
                for(int number = LOWERBOUND; number <= UPPERBOUND; ++number){
                    sum += number;
                }
                
                average = sum / UPPERBOUND;
                
                System.out.println("The sum of 1 to 100 is " + sum);
                System.out.println("The average is " + average);
                
                break;
            case 2:
                int number = LOWERBOUND;
                while(number <= UPPERBOUND){
                    sum += number;
                    ++number;
                }
                
                average = sum / UPPERBOUND;
                
                System.out.println("The sum of 1 to 100 is " + sum);
                System.out.println("The average is " + average);
                
                break;
            case 3:
                int number1 = LOWERBOUND;
                do{
                    sum += number1;
                    ++number1;
                }while(number1 <= UPPERBOUND);
                
                average = sum / UPPERBOUND;
                
                System.out.println("The sum of 1 to 100 is " + sum);
                System.out.println("The average is " + average);
                
                break;
            case 4:
                for(int number2 = LOWERBOUND1; number2 <= UPPERBOUND1; 
                        ++number2){
                    sum += number2;
                    ++count;
                }
                
                average = sum / count;
                
                System.out.println("The sum of 111 to 8899 is " + sum);
                System.out.println("The average is " + average);
                
                break;
            case 5:
               int number3 = LOWERBOUND;
               int square;
               
               do{
                   square = number3 * number3;
                   sum += square;
                   ++number3;
               }while(number3 <= UPPERBOUND);
               
               System.out.println("The sum of the squares: " + sum);
               
               break;
            case 6:
                int sumOdd = 0;
                int sumEven = 0;
                int absDiff;
                
                for(int number4 = LOWERBOUND; number4 <= UPPERBOUND; ++number4){
                    if((number4 % 2) != 0){
                        sumOdd += number4;
                    }
                    else{
                        sumEven += number4;
                    }
                }
                
                if(sumOdd > sumEven){
                    absDiff = sumOdd - sumEven;
                }
                else{
                    absDiff = sumEven - sumOdd;
                }
                
                System.out.println("The sum of all Odd Numbers: " + sumOdd);
                System.out.println("The sum of all Even Numbers: " + sumEven);
                System.out.println("The Absolute difference between "
                        + "the two sums: " + absDiff);
                
                break;
        }
        
        getChoice.close();
    }
}
