package devkinetics;

import java.util.Scanner;

public class IncomeTaxCalculator {
    public static void main(String[] args) {
        final double TAX_RATE_ABOVE_20K = 0.1;
        final double TAX_RATE_ABOVE_40K = 0.2;
        final double TAX_RATE_ABOVE_60K = 0.3;

        int choice;
        int taxableIncome = 0;
        double taxPayable = 0;
        
        Scanner getIncome = new Scanner(System.in);
        
        System.out.println("Choose how you want to compute tax");
        System.out.println("1. Without Rebate");
        System.out.println("2. With Rebate");
        System.out.println("Choice: ");
        
        choice = getIncome.nextInt();
        
        switch(choice){
            case 1:
                System.out.print("Enter the taxable income: $");
                taxableIncome = getIncome.nextInt();

                if (taxableIncome <= 20000) {
                    taxPayable = taxableIncome * 0;
                } else if (taxableIncome <= 40000) {
                    taxableIncome -= 20000;
                    taxPayable += taxableIncome * TAX_RATE_ABOVE_20K;
                } else if (taxableIncome <= 60000) {
                    taxableIncome -= 20000;
                    taxableIncome -= 20000;
                    taxPayable += 20000 * TAX_RATE_ABOVE_20K;
                    taxPayable += taxableIncome * TAX_RATE_ABOVE_40K;
                } else {
                    taxableIncome -= 20000;
                    taxableIncome -= 20000;
                    taxableIncome -= 20000;
                    taxPayable += 20000 * TAX_RATE_ABOVE_20K;
                    taxPayable += 20000 * TAX_RATE_ABOVE_40K;
                    taxPayable += taxableIncome * TAX_RATE_ABOVE_60K;
                }

                System.out.printf("The income tax payable is: $%.2f%n", 
                        taxPayable);
                
                break;
            case 2:
                System.out.print("Enter the taxable income: $");
                taxableIncome = getIncome.nextInt();

                if (taxableIncome <= 20000) {
                    taxPayable = taxableIncome * 0;
                } else if (taxableIncome <= 40000) {
                    taxableIncome -= 20000;
                    taxPayable += taxableIncome * TAX_RATE_ABOVE_20K;
                } else if (taxableIncome <= 60000) {
                    taxableIncome -= 20000;
                    taxableIncome -= 20000;
                    taxPayable += 20000 * TAX_RATE_ABOVE_20K;
                    taxPayable += taxableIncome * TAX_RATE_ABOVE_40K;
                } else {
                    taxableIncome -= 20000;
                    taxableIncome -= 20000;
                    taxableIncome -= 20000;
                    taxPayable += 20000 * TAX_RATE_ABOVE_20K;
                    taxPayable += 20000 * TAX_RATE_ABOVE_40K;
                    taxPayable += taxableIncome * TAX_RATE_ABOVE_60K;
                }

                System.out.printf("The income tax payable is: $%.2f%n", 
                        taxPayable);
                if((taxPayable * 0.1) > 1000){
                    System.out.println("Rebate: $1000");
                    taxPayable = 1000;
                    System.out.printf("New tax payable with rebate: $%.2f%n",
                            taxPayable);
                }else{
                    System.out.printf("Rebate: $%.2f%n",  taxPayable * 0.1);
                    taxPayable -= taxPayable * 0.1;
                    System.out.printf("New tax payable with rebate: $%.2f%n",
                            taxPayable);
                }
                
                
                break;
        }

        getIncome.close();
    }
}
