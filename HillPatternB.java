package devkinetics;

import java.util.Scanner;

public class HillPatternB {
    public static void main(String[] args){
        int numRows, numCol;
        
        Scanner getSize = new Scanner(System.in);
        
        System.out.print("Enter the rows: ");
        numRows = getSize.nextInt();
        
        for (int row = numRows; row >= 1; --row) {
            numCol = 2*numRows - 1;
            for (int col = numCol; col >= 1; --col) {
                if ((row + col >= numRows + 1) && (row >= col - numRows + 1)) {
                    System.out.print(" # ");
                } else {
                   System.out.print("   ");
                }
            }
            System.out.println();
        }
    }
}
