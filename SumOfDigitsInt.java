package devkinetics;

import java.util.Scanner;

public class SumOfDigitsInt {
    public static void main(String[] args){
        int inNumber;
        int inDigit;
        int sum = 0;
        
        Scanner getInput = new Scanner(System.in);
        
        System.out.print("Enter a positive integer: ");
        inNumber = getInput.nextInt();
        
        while(inNumber > 0){
            inDigit = inNumber % 10;
            
            sum += inDigit;
            
            inNumber /= 10;
        }
        
        System.out.println("The sum of all digits is: " + sum);
    }
}
