package devkinetics;

import java.util.Scanner;

public class CheckPassFail {
    public static void main(String[] args){
        int mark;
        Scanner getMark = new Scanner(System.in);
        
        System.out.print("Enter the mark: ");
        mark = getMark.nextInt();
        
        getMark.close();
        
        System.out.println();
        
        System.out.println("The mark is " + mark);
        System.out.print("Remark: ");
        
        if(mark >= 50){
            System.out.println("PASS");
        }
        else{
            System.out.println("FAIL");
        }
        System.out.println("DONE!");
    }
}
